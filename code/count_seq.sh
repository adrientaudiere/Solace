#!/bin/bash
cd $1

rm -f /tmp/temp.txt
touch /tmp/temp.txt
for files in *_R1_001.fastq.gz 
do
    zcat $files | grep -c '^+$' >> /tmp/temp.txt
done
paste -sd+ /tmp/temp.txt | bc
