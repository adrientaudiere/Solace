source ~/miniconda3/etc/profile.d/conda.sh # solution pour contourner le conda init qui demande de relancer le shell
conda activate cutadaptenv

cd /home/adrien/Bureau/Solace/data/raw_seq 
mkdir -p /home/adrien/Bureau/Solace/data/raw_seq/wo_primers/ITS
mkdir -p /home/adrien/Bureau/Solace/data/raw_seq/wo_primers/16S
mkdir -p /home/adrien/Bureau/Solace/data/raw_seq/wo_primers/AM


for i in *_R1_001.fastq.gz
do
  SAMPLE=$(echo ${i} | sed "s/_R1_\001\.fastq\.gz//")
  echo ${SAMPLE}_R1_001.fastq.gz ${SAMPLE}_R2_001.fastq.gz
cutadapt --cores=4 --discard-untrimmed -g "GTGAATCATCGAATCTTTGAA" -G "TCCTCCGCTTATTGATATGC" -o wo_primers/ITS/${SAMPLE}_R1_001.fastq.gz -p wo_primers/ITS/${SAMPLE}_R2_001.fastq.gz ${SAMPLE}_R1_001.fastq.gz ${SAMPLE}_R2_001.fastq.gz --json=wo_primers/ITS/${SAMPLE}_cutadapt.json --report=minimal
done


for i in *_R1_001.fastq.gz
do
  SAMPLE=$(echo ${i} | sed "s/_R1_\001\.fastq\.gz//")
  echo ${SAMPLE}_R1_001.fastq.gz ${SAMPLE}_R2_001.fastq.gz
cutadapt --cores=4 --discard-untrimmed -g "CCTAYGGGRBGCASCAG" -G "GGACTACNNGGGTATCTAAT" -o wo_primers/16S/${SAMPLE}_R1_001.fastq.gz -p wo_primers/16S/${SAMPLE}_R2_001.fastq.gz ${SAMPLE}_R1_001.fastq.gz ${SAMPLE}_R2_001.fastq.gz --json=wo_primers/16S/${SAMPLE}_cutadapt.json --report=minimal
done



for i in *_R1_001.fastq.gz
do
  SAMPLE=$(echo ${i} | sed "s/_R1_\001\.fastq\.gz//")
  echo ${SAMPLE}_R1_001.fastq.gz ${SAMPLE}_R2_001.fastq.gz
cutadapt --cores=4 --discard-untrimmed -g "AAGCTCGTAGTTGAATTTCG" -G "CCCAACTATCCCTATTAATCAT" -o wo_primers/AM/${SAMPLE}_R1_001.fastq.gz -p wo_primers/AM/${SAMPLE}_R2_001.fastq.gz ${SAMPLE}_R1_001.fastq.gz ${SAMPLE}_R2_001.fastq.gz --json=wo_primers/AM/${SAMPLE}_cutadapt.json --report=minimal
done
