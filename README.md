# Solace

## Requirement

-   [vsearch](https://github.com/torognes/vsearch) software installed
-   \~30Go of RAM for some steps (in particular for taxonomic assignation) but you can use swapfile to easily increase it.
-   cutapapt installed using miniconda installer
-   Modify cutadapt.sh depending on your primers and folder and run on your sequences data (18790084 sequences before and 18459612 sequences after cutadapt)
-   R and packages includes in the file code/packages.R

## Prérequis

-   Installer [vsearch](https://github.com/torognes/vsearch)
-   Certaines étapes (assignations taxonomiques) nécessitent beaucoup de mémoire vive (\~30Go). Une solution est de créer un gros fichier swapfile pour avoir une mémoire swap en soutien de la RAM.
-   Installer cutapapt en utilisant miniconda
-   Modifier cutadapt.sh en fonction de vos paramètres d'amorces et de dossier et lancer le sur vos séquences (18790084 séquences avant et 18459612 séquences après cutadapt)
-   Installer R (plusieurs packages listés dans le fichier code/packages.R seront automatiquement installés)


## Other tools 

### Quality check

Quality check may be done using FastQC (for ex. using ) or [falco](https://github.com/smithlabcode/falco) software. 
[MultiQC](https://multiqc.info/) is usefull for aggregating bioinformatic logs from fastqc or cutadapt.

### Falco

- Installing falco

```sh
conda install -c bioconda falco
```

- Runnig falco 

```sh
falco data/raw_seq/*.fastq.gz
```
